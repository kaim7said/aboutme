var indexchartskill = 0;
//var skillh = 0;
//var abouth = 0;

function SkillBind(skills)
{
	$('div#skillcontainer').empty();
	var htmlskrow = '';
	$.each(skills, function(i, itemsk) {
		htmlskrow = '<div class="col-md-4 col-sm-6 skill"> <span class="chart" data-percent="' + itemsk.levelskill + '"> <span class="percent">' + itemsk.levelskill + '</span> </span>';
		htmlskrow += '<h4>' + itemsk.nameskill + '</h4></div>';
		
		$('div#skillcontainer').append(htmlskrow);
		
	});

	
	
	$(document).scroll(function(){
		ChartSkillActive();
	});


}

function ChartSkillActive()
{
	var topwin = $(window).scrollTop() + 400;
	//var topsk = skillh-$(window).scrollTop();
	//var topab = abouth-$(window).scrollTop();
	var skillh = $('#skills').offset().top;
	//skillh = $('#skills').height();
	var abouth = $('#about').offset().top;
	//console.log((skillh - abouth) + ' <-> ' + abouth + ' <-> ' + (topwin - abouth));
	if((skillh - abouth) < (topwin - abouth)){
		if(indexchartskill==0){	
		
			$('.chart').easyPieChart({
				easing: 'easeOutBounce',
				animate: 8000,
				onStep: function(from, to, percent) {
					$(this.el).find('.percent').text(Math.round(percent));
				}
			});
		}
		indexchartskill++;
	}
}

function PortfolioBind(portfolios)
{
	$('ol#categoriesportfolio').empty();
	$('ol#categoriesportfolio').append('<li><a href="#" data-filter="*" class="active">All</a></li>');
	var htmlportrow = '';
	var htmlportitem = '';
	$('div.portfolio-items').empty();
	
	$.each(portfolios, function(i, categoriaport) {
		htmlportrow = '<li><a href="#" data-filter=".' + categoriaport.portfoliocategorieid + '">' + categoriaport.portfoliocategoriename + '</a></li>';
		$('ol#categoriesportfolio').append(htmlportrow);
		
		$.each(categoriaport.portfolioitems, function(i, itemport) {
			htmlportitem = '<div class="col-sm-6 col-md-3 col-lg-3 ' + categoriaport.portfoliocategorieid + '">';
			htmlportitem += '<div class="card portfolio-item">';
			htmlportitem += '<div class="hover-bg card-body">';
			htmlportitem += '<a href="' + itemport.linkportfolio + '" title="Project description" target="_blank" rel="prettyPhoto">';
			//htmlportitem += '<div class="card-body">';
			htmlportitem += '<h4 class="card-title"><strong>' + itemport.nameportfolio + '</strong></h4>';
			htmlportitem += '<p class="card-text">' + itemport.descportfolio + '</p>';
			htmlportitem += '</a></div></div></div>';
			//htmlportitem += '</div></a></div></div></div>';
			
			$('div.portfolio-items').append(htmlportitem);
		});
		
	});
}

function SomeStatsBind(somestats)
{
	$('div#somestatscontainer').empty();
	
	$.each(somestats, function(i, itemst) {
		var htmlstatsrow = '';
		htmlstatsrow += '<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="' + itemst.delay + '">';
		htmlstatsrow += '<div class="achievement-box">';
		htmlstatsrow += '<span class="count">' + itemst.value + '</span>';
		htmlstatsrow += '<h4>' + itemst.name + '</h4>';
		htmlstatsrow += '</div></div>';

		$('div#somestatscontainer').append(htmlstatsrow);
		
	});

	if($("span.count").length > 0){	
		$('span.count').counterUp({
				delay: 10, // the delay time in ms
		time: 10500 // the speed time in ms
		});
	}
}


function XpTimeLineBind(xptimeline)
{
	$('ul#xptimeline').empty();
	
	$.each(xptimeline, function(i, itemxptm) {
		var htmlxptmrow = '';

		if (i % 2 == 0){
			htmlxptmrow += '<li>';
		}else{
			htmlxptmrow += '<li class="timeline-inverted">';
		}

		htmlxptmrow += '<div class="timeline-image">';
		htmlxptmrow += '<h4>' + itemxptm.period.datein + ' <br>';
		htmlxptmrow += '- <br>' + itemxptm.period.dateout + ' </h4></div>';
		htmlxptmrow += '<div class="timeline-panel"><div class="timeline-heading">';
		htmlxptmrow += '<h4>' + itemxptm.employer + '</h4>';
		htmlxptmrow += '<h4 class="subheading">' + itemxptm.ocupation + '</h4>';
		htmlxptmrow += '</div><div class="timeline-body">';
		htmlxptmrow += '<p>' + itemxptm.description + '</p>';
		htmlxptmrow += '</div></div></li>';
	
		
		$('ul#xptimeline').append(htmlxptmrow);
		
	});
}


function EduTimeLineBind(edutimeline)
{
	$('ul#edutimeline').empty();
	
	$.each(edutimeline, function(i, itemedutm) {
		var htmledutmrow = '';

		if (i % 2 == 0){
			htmledutmrow += '<li>';
		}else{
			htmledutmrow += '<li class="timeline-inverted">';
		}

		htmledutmrow += '<div class="timeline-image">';
		//htmledutmrow += '<h4>' + itemedutm.period.datein + ' <br>';
		//htmledutmrow += '- <br>' + itemedutm.period.dateout + ' </h4></div>';
		htmledutmrow += '<h4>' + itemedutm.period.dateout + ' </h4></div>';
		htmledutmrow += '<div class="timeline-panel"><div class="timeline-heading">';
		htmledutmrow += '<h4>' + itemedutm.institution + '</h4>';
		htmledutmrow += '<h4 class="subheading">' + itemedutm.course + '</h4>';
		htmledutmrow += '</div><div class="timeline-body">';
		htmledutmrow += '<p>' + itemedutm.description + '</p>';
		htmledutmrow += '</div></div></li>';
	
		
		$('ul#edutimeline').append(htmledutmrow);
		
	});
}

function SocialNetwork(socialnetwork)
{
	$('div.social-network > ul').empty();
	
	$.each(socialnetwork, function(i, itemsnet) {
		var htmlsocialnetrow = '';
		htmlsocialnetrow += '<li><a href="' + itemsnet.url + '">';
		htmlsocialnetrow += '<i class="fa ' + itemsnet.icon + '"></i></a></li>';
		
		$('div.social-network > ul').append(htmlsocialnetrow);
		
	});
	
}


function BindPage(resume){
	'use strict';
	$('html').smoothScroll(900);
	
	//console.log(resume);
	document.title = resume.name + " - " + resume.profile;
	$('span.name').text(resume.name);
	$('p.profile').text(resume.profile);
	$('a.name').text(resume.name);
	$('div.about-text > p.resumedescription').text(resume.description);
	$('div.intro').css('background-image', 'url(' + resume.urlimgbanner + ')');
	$('img#imgaboutme').attr("src",resume.urlimgaboutme);
	
	
	$('a#btnResumeDownload').click(function(e) {
		e.preventDefault();  //stop the browser from following
		window.location.href = 'uploads/' + resume.Urlresumedownload;
	});
	
	//contact
	//$('p#contactaddress').text(resume.AddressContact);
	
	//$('a#contactmaps').attr("href", resume.AddressUrlGmaps);
	$('p#contactmail').text(resume.emailcontact);
	//$('p#contactphonenumber').text(resume.PhoneNumberContact);

	
	SkillBind(resume.skills);

	ChartSkillActive();
	$(document).scroll(function(){
		ChartSkillActive();
		var scrollDistance = $(this).scrollTop();
		if (scrollDistance > 100) {
		  $('.scroll-to-top').fadeIn();
		} else {
		  $('.scroll-to-top').fadeOut();
		}
	});

	PortfolioBind(resume.portfolios);
	SomeStatsBind(resume.somestats);

	XpTimeLineBind(resume.experiences);

	EduTimeLineBind(resume.educations);

	SocialNetwork(resume.socialnetworks);
	
	
   
  	$('a.page-scroll').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top //- 40
            }, 900);
            return false;
          }
        }
		$('#navbarSupportedContent').collapse('hide');
      });

	// Closes responsive menu when a scroll trigger link is clicked
	$('a.navbar-brand.page-scroll.name').click(function() {
		$('#navbarSupportedContent').collapse('hide');
	});
	// Closes responsive menu when a scroll trigger link is clicked
	$('a.page-scroll.scroll-to-top').click(function() {
		$('#navbarSupportedContent').collapse('hide');
	});
	$('#navbarSupportedContent a.page-scroll').click(function() {
		$('#navbarSupportedContent').collapse('hide');
	});
	  
	// affix the navbar after scroll below header
	//$('#nav').affix({
	//	  offset: {
	//		top: $('header').height()
	//	  }
	//});	
	
	setTimeout(function(){
    //do what you need here
	
		// Portfolio isotope filter
		var $container = $('.portfolio-items');
		//$container.isotope({ filter: '*' });
		$container.isotope({
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
		$('.cat a').click(function() {
			$('.cat .active').removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$container.isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'linear',
					queue: false
				}
			});
			return false;
		});
	}, 3000);
	// Pretty Photo
	//$("a[rel^='prettyPhoto']").prettyPhoto({
	//	social_tools: false
	//});	
}

function GetResume(param){
	$(".resumeclassabout").hide();
	$('.modal').modal('show');
	$.ajax({
	  url: "https://knowmoreaboutme.tk:8081/cv/resume/" + param,
	  method: "get",
	  datatype: "json",
	  success: function(objdata){
		 //console.log(objdata);
		 BindPage(objdata);
		$(".resumeclassabout").show();
		$('.modal').modal('hide');
	  },
	  error: function(){
		alert('failure');
		$('.modal').modal('hide');
	  }
	});
}
$(document).ready(function() {
	GetResume("khaled")
	//BindPage(resumemock);
	
});




